<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = [
        'customer_name',
        'branch_name',
        'points'
    ];

    protected $table = 'customers';
}
