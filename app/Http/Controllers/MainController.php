<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modules\Customer;
use App\Modules\User;

class MainController extends Controller
{
    public function getCustomerPage()
    {
        return Customer::getPage(true);
    }

    public function getUserPage()
    {
        return User::getPage(true);
    }
}
