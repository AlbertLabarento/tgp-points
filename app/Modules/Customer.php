<?php

namespace App\Modules;

use App\Customer as Model;
use Redirect;

class Customer{

    public static function getPage($isView = false, $data = []){
        return self::redirect('pages.customers', $isView, $data);
    }

    public static function toPageData( $id, $isView = false )
    {
        return self::redirect('pages.customer_points', $isView, Model::find($id));
    }

    public static function redirect( $page, $isView = false, $data = [] )
    {
        $customers = Model::all();
        $with = compact('page', 'customers');
        if(!empty($data))
            $with = compact('page', 'customers', 'data');

        if($isView){
            return view('tgp')->with($with);
        } else {
            $url = '/';
            if(!empty($data) && $page == 'pages.customer_points'){
                $url += 'CustomerPoints/'.$data->id;
            }
            return Redirect::to($url)->withInputs($with);
        }
    }

}