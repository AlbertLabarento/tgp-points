<?php

namespace App\Modules;

use App\User as Model;
use Redirect;

class User{

    public static function getPage($isView = false, $data = []){
        $page = 'pages.users';
        $users = Model::all();
        $with = compact('page', 'users');
        if(!empty($data))
            $with = compact('page', 'users', 'data');

        if($isView){
            return view('tgp')->with($with);
        } else {
            return Redirect::to('/Users')->withInputs($with);
        }
    }

}