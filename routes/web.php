<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware'=>'auth'], function(){
    Route::get('/', 'MainController@getCustomerPage')->name('customers');
    Route::get('/Users', 'MainController@getUserPage')->name('users');
    Route::get('CustomerPoints/{id}', 'CustomerController@claimOrRedeemPoints')->name('customer_points');
    Route::put('ClaimOrRedeem/{id}', 'CustomerController@putClaimOrRedeemPoints')->name('claim_redeem');
    Route::post('/AddUser', 'Auth\RegisterController@create')->name('add_users');
    Route::resource('customer', 'CustomerController', ['as' => 'customers']);
    Route::resource('user', 'UserController', ['as' => 'users']);
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
