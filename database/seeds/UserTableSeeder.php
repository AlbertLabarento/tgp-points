<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        User::create([
            'name' => 'Bryan',
            'email' => 'edwardbryan.abergas@gmail.com',
            'password' => bcrypt('qweasd')
        ]);
    }
}
