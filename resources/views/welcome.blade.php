<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>TGP</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        {{ Html::style('vendor/bootstrap/css/bootstrap.min.css') }}
        {{ Html::style('vendor/font-awesome/css/font-awesome.min.css') }}
        {{ Html::style('vendor/datatables/dataTables.bootstrap4.css') }}
        {{ Html::style('css/sb-admin.css') }}
        </style>
    </head>
    <body class="fixed-nav" id="page-top">
         @include('navigation')
         @include('content')
    </body>

    {{ Html::script('vendor/jquery/jquery.min.js') }}
    {{ Html::script('vendor/popper/popper.min.js') }}
    {{ Html::script('vendor/bootstrap/js/bootstrap.min.js') }}
    {{ Html::script('vendor/jquery-easing/jquery.easing.min.js') }}
    {{ Html::script('vendor/chart.js/Chart.min.js') }}
    {{ Html::script('vendor/datatables/jquery.dataTables.js') }}
    {{ Html::script('vendor/datatables/dataTables.bootstrap4.js') }}
    {{ Html::script('js/sb-admin.min.js') }}
<script>
(() => {
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('input[name="_token"]').attr('value')
    }
});
$(document).on('click', 'a.postback', function(e) {
    e.preventDefault(); // does not go through with the link.

    var $this = $(this);
    console.log($this.data('method'));
    $.ajax({
        type: $this.data('method'),
        url: $this.attr('href'),
        headers: {
            'X-CSRF-TOKEN': $('meta[name="token"]').attr('content')
        },
        data : {
            'token' : $('meta[name="token"]').attr('content')
        }
    }).done(function (data) {
        location.reload();
    });
});
})();
</script>
</html>
