
@if(!\Route::is('customers.customer.edit'))
<h3>Customers</h3>
<form class="form-inline" method="POST" action="{{route('customers.customer.store')}}">
  <div class="form-group col-md-3">
    <label class="col-sm-4 control-label">Full name</label>
    <div class="col-sm-8">
    <input type="text" name="customer_name" class="form-control"placeholder="Full name">
    </div>
  </div>
  <div class="form-group col-md-3">
    <label class="col-sm-3 control-label">Branch</label>
    <div class="col-sm-9">
        <select name="branch_name" id="" class="form-control">
            <option>Marilao</option>
            <option>Quatro Kanto</option>
            <option>Taytay</option>
        </select>
    </div>
  </div>
  <div class="form-group col-md-3">
    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
    <button type="submit" class="btn btn-primary">Add</button>
  </div>
</form>
@else
<h3>Customer {{$data->customer_name}}</h3>
<form class="form-inline" method="POST" action="{{route('customers.customer.update', $data->id)}}">
  <div class="form-group col-md-3 ">
    <label class="col-sm-4 control-label">Full name</label>
    <div class="col-sm-8">
    <input type="text" name="customer_name" class="form-control"placeholder="Full name" value="{{$data->customer_name}}">
    </div>
  </div>
  <div class="form-group col-md-3">
    <label class="col-sm-3 control-label">Branch</label>
    <div class="col-sm-9">
        <select name="branch_name" id="" class="form-control">
            <option @if( $data->branch_name == 'Marilao')selected=""@endif>Marilao</option>
            <option @if( $data->branch_name == 'Quatro')selected=""@endif>Quatro Kanto</option>
            <option @if( $data->branch_name == 'Taytay')selected=""@endif>Taytay</option>
        </select>
    </div>
  </div>
  <div class="form-group col-md-3">
    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
    <input type="hidden" name="_method" value="PUT">
    <button type="submit" class="btn btn-primary">Save</button>
  </div>
</form>
@endif

<table class="table col-md-12">
  <thead> 
    <tr> 
      <th>Name</th> 
      <th>Branch Name</th> 
      <th>Points</th> 
      <th>Actions</th> 
    </tr> 
  </thead>
  <tbody>
    @foreach( $customers as $customer )
      <tr>
        <td>{{$customer->customer_name}}</td>
        <td>{{$customer->branch_name}}</td>
        <td>{{$customer->points}}</td>
        <td>
          @if(!\Route::is('customers.customer.edit'))<a role="button" href="{{route('customers.customer.destroy', $customer->id)}}" data-method="DELETE" class="postback"><span class="fa fa-trash text-danger"></span></a>&nbsp;&nbsp;&nbsp;@endif
          <a role="button" href="{{route('customers.customer.edit', $customer->id)}}"><span class="fa fa-edit"></span></a> &nbsp;&nbsp;&nbsp
          <a role="button" class="btn btn-info btn-sm" href="{{route('customer_points', $customer->id)}}">Claim / Redeem Points</a> 
        </td>
      </tr>  
    @endforeach
  </tbody> 
</table>