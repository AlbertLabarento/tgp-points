<div class="container">
<h3>Customer Points {{$data->customer_name}}</h3>
  <form method="POST" action="{{route('claim_redeem', $data->id)}}">
    <div class="form-group row">
      <label class="col-sm-4 control-label">Current Points</label>
      <div class="col-sm-10">
        <input type="text" disabled class="form-control" placeholder="Points" value="{{$data->points}}">
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-4 control-label">Points</label>
      <div class="col-sm-10">
        <input type="text" name="points" class="form-control" placeholder="Points" >
      </div>
    </div>
    <fieldset class="form-group row">
      <div class="col-sm-10">
        <div class="form-check">
          <label class="form-check-label">
             <input type="radio" name="type" placeholder="Points" value="claim" checked>
             Claim
          </label>
        </div>
        <div class="form-check">
          <label class="form-check-label">
             <input type="radio" name="type" placeholder="Points" value="redeem">
             Redeem
          </label>
        </div>
    </fieldset>
    <div class="form-group row">
      <div class="offset-sm-2 col-sm-10">
        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
        <input type="hidden" name="_method" value="PUT">
        <button type="submit" class="btn btn-primary">Claim / Redeem</button>
      </div>
    </div>
  </form>
</div>