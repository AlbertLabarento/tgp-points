@if(!\Route::is('users.user.edit'))
<h3>Users</h3>
<form class="form-inline" action="{{route('users.user.store')}}" method="POST">
  <div class="form-group col-md-2">
    <div class="col-sm-8">
    <input type="text" name="name" class="form-control"placeholder="Full name">
    </div>
  </div>
  <div class="form-group col-md-2">
    <div class="col-sm-10">
    <input type="email" name="email" class="form-control" placeholder="Email">
    </div>
  </div>
  <div class="form-group col-md-2">
    <div class="col-sm-9">
      <input type="password" name="password" class="form-control" placeholder="Password">
    </div>
  </div>
  <div class="form-group col-md-2">
    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
    <button type="submit" class="btn btn-primary">Add</button>
  </div>
</form>
@else
<h3>User {{$data->name}}</h3>
<form class="form-inline" method="POST" action="{{route('users.user.update', $data->id)}}">
  <div class="form-group col-md-3">
    <div class="col-sm-8">
    <input type="text" name="name" class="form-control"placeholder="Full name" value="{{$data->name}}">
    </div>
  </div>
  <div class="form-group col-md-3">
    <div class="col-sm-10">
    <input type="email" name="email" class="form-control" placeholder="Email" value="{{$data->email}}">
    </div>
  </div>
  <div class="form-group col-md-3">
    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
    <input type="hidden" name="_method" value="PUT">
    <button type="submit" class="btn btn-primary">Save</button>
  </div>
</form>
@endif


<table class="table col-md-12">
  <thead> 
    <tr> 
      <th>Name</th> 
      <th>Email</th> 
      <th>Actions</th> 
    </tr> 
  </thead>
  <tbody>
    @foreach( $users as $user )
      <tr>
        <td>{{$user->name}}</td>
        <td>{{$user->email}}</td>
        <td>
          @if(!\Route::is('users.user.edit'))<a role="button" href="{{route('users.user.destroy', $user->id)}}" data-method="DELETE" class="postback"><span class="fa fa-trash text-danger"></span></a>&nbsp;&nbsp;&nbsp;@endif
          <a role="button" href="{{route('users.user.edit', $user->id)}}"><span class="fa fa-edit"></span></a> 
        </td>
      </tr>  
    @endforeach
  </tbody> 
</table>
