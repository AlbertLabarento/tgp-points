<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
      <a class="navbar-brand" href="#">TGP Points</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav navbar-sidenav">
          <li class="nav-item {{ Route::is('customers') ? 'active' : '' }}" data-toggle="tooltip" data-placement="right" title="" data-original-title="Charts">
            <a class="nav-link" href="{{route('customers')}}">
              <i class="fa fa-fw fa-users"></i>
              <span class="nav-link-text">
                Customers</span>
            </a>
          </li>
          <li class="nav-item {{ Route::is('users') ? 'active' : '' }}" data-toggle="tooltip" data-placement="right" title="" data-original-title="Dashboard">
            <a class="nav-link" href="{{route('users')}}">
              <i class="fa fa-fw fa-user"></i>
              <span class="nav-link-text">
                Users</span>
            </a>
          </li>
        </ul>
      </div>
    </nav>